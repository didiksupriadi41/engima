<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?= BASEURL ?>/public/css/navbar.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
</head>
<body>
    <ul class="navbar">
        <li id="engima">
            <a href="<?= BASEURL ?>/Homepage">
                <span id="engi">Engi</span><span id="ma">ma</span>
            </a>
        </li>
        <li id="search-bar">
            <div class="button-wrap">
                <form action="<?= BASEURL ?>/Searchpage/findMovie" method="GET">
                    <input class="search-input" type="text" placeholder="Search movie" name="search-input" id="search-input">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </li>
        <li id="logout">
            <a href="<?= BASEURL ?>/Logout">
                Logout
            </a>
        </li>
        <li id="transaction">
            <a href="<?= BASEURL ?>/Transaction">
                Transactions
            </a>
        </li>
    </ul>
</body>
</html>
