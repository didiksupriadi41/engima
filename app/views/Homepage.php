<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?= BASEURL ?>/public/css/homepage.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <title>Homepage</title>
</head>

<body>
    <div class = "container">
        <div class = "main-page">
            <div id = "user-text"> 
                <h2> Hello, <span id = "user-name"> <?= ($this->model('LoginModel')->findWhoIsLogin())?></span>! </h2>
                <h3> Now Playing </h3>
            </div>
            <div class="flex-container">
                <?php foreach($data['query'] as $film): ?>
                    <form action="<?= BASEURL?>/Details/movieDetails" method = "GET">
                        <button type="submit" name="view-movie" class="ref-details" id = "poster-homepage" value = "<?= $film['IDFilm']?>">
                            <img src = "<?= $film['Film_Foto']?>" ">
                        </button>
                        <div class = "desc" >
                            <form action="<?= BASEURL?>/Details/movieDetails" method = "GET">
                                <button type = "submit"  name="view-movie" class="ref-details" id = "text-homepage" value = "<?= $film['IDFilm']?>">
                                    <?= $film['Judul'] ?>
                                </button>
                            </form>
                            <div>
                                <img id = "rating-movie" src = "<?= BASEURL ?>/public/img/star.png">
                                <?= $film['Rating'] ?>
                            </div>
                        </div>
                    </form>
                <?php endforeach; ?>
            </div>
            
        </div>
    </div>
</body>
</html>