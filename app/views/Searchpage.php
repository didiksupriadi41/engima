<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?= BASEURL ?>/public/css/searchpage.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <title>Searchpage</title>
</head>

<body>
    <div class="container">
        <div id="transaction">Showing search result for keyword "<?=$_GET['search-input'] ?>"</div>
        <div id="showresult"><?= $data['countrow'] ?> results available</div>
        <ul>
            <?php foreach( $data['query'] as $film ) : ?>  
            <li>
                <div id="flex-container">
                    <div class="leftcontent">
                        <img src="<?= $film['Film_Foto'] ?>" class="film-image">
                    </div>
                    <div class="rightcontent">
                        <div id="judul-content">
                            <?= $film['Judul'] ?>
                        </div>
                        <i class="fa fa-star"><div id="rating-content"><?= $film['Rating']?></div></i>
                        <div id="sinopsis-content"><?= $film['Sinopsis'] ?></div>
                    </div>
                </div>
                <div class="view-details">
                    <form action="<?= BASEURL?>/Details/movieDetails" method="GET">
                        <button type="submit" name="view-movie" class="ref-details" value="<?= $film['IDFilm']?>">View Details</button>
                    </form>
                    <i class="fa fa-chevron-circle-right"></i></a>
                </div>
                <div class="border-bottom"></div>
            </li>
            <?php endforeach; ?>
        </ul>
        <div id="pagination">
            <form action="" method="POST">
                <button type="submit" name="back-page" class="page-button" value="-5">Back</button>
                <?php
                    $pages = ceil($data['countrow']/5);
                    for ($i=0 ; $i<$pages ; $i++) { ?>
                        <button type="submit" name="page" class="page-number" value="<?=$i*5?>"> <?=$i+1?></button>            
                    <?php }            
                ?>
                <button type="submit" name="next-page" class="page-button" value="5">Next</button>
            </form>
        </div>
    </div>
</body>
</html>