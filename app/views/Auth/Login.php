<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Login</title>
    <link href="<?= BASEURL ?>/public/css/login.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="box">
        <form action="<?= BASEURL ?>/Login/checkAccount" method="POST">
            <h3 class="text-center"> Welcome to <span class="engi">Engi</span>ma </h3>
            <div class="container">
                <label for="email"> <p> Email </p> </label> 
                <input type = "text" placeholder="irfansofyana@gmail.com" name="email" required
                <?php
                    if (isset($data['failed'])){
                        echo "value = ".$data['email'];
                    }
                ?>>


                <label for="psw"> <p> Password </p> </label> 
                <input type = "password" placeholder="place here" name="psw" required
                <?php
                    if (isset($data['failed'])){
                        echo "value = ".$data['psw'];
                    }
                ?>>
                <br>

                <div class = "error-message">
                    <?php
                        if (isset($data['failed'])){
                            echo " <div class = 'alert-danger'> 
                                        Your Email or Password is Wrong!
                                    </div>
                                    ";
                        }
                    ?>
                </div>
                <button type = "submit"> Login </button>
                <p class="text-center" id="text-bottom"> <b> Don't have an account? </b> <a id="register-text" href="<?= BASEURL ?>/Register"> <b> Register here </b></a> </p>
            </div>
        </form>
    
    </div>
</body>

</html>
