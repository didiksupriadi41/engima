<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link href="<?= BASEURL ?>/public/css/register.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <script>
        function uploadFile(_file){
            var filename = _file.value;
            var lastIndex = filename.lastIndexOf("\\");
            if (lastIndex >= 0) 
                filename = filename.substr(lastIndex+1);
            document.getElementById("file-uploaded").value = filename;
        }
    </script>
</head>

<body>
    <div class="box">
        <h3 id = "text-welcome" class="text-center"> Welcome to <span id="engi">Engi</span>ma </h3>

        <form action="<?= BASEURL ?>/Register/test" method = "POST" enctype = "multipart/form-data">
            <div class = "container">
                <label for = "username"> <p class="text-label"> Username </p> </label> 
                <input type = "text" placeholder="irfansofyana" name="username" required>

                <label for = "email"> <p class="text-label"> Email </p> </label>
                <input type = "text" placeholder="irfansofyana@gmail.com" name="email" required>

                <label for = "phoneNumber"> <p class="text-label"> Phone Number </p> </label>
                <input type = "tel" placeholder="+62811123xxxx" name="phoneNumber" required>

                <label for = "password"> <p class="text-label"> Password </p> </label>
                <input type = "password" placeholder="make as strong as possible" name="password" required>

                <label for = "cPassword"> <p class="text-label"> Confirm Password </p> </label>
                <input type = "password" placeholder="same as above" name = "cPassword" required>
            

                <label for = "file"> <p class="text-label"> Profile Picture </p> </label>

                <div class = "file-container">
                    <input disabled type="text" id="file-uploaded"> 
                    <label class="upload-container">
                        <button type="btn"> 
                            Browse
                        </button>
                        <input type="file" name = "file" id = "file" onChange="uploadFile(this)" accept = "image/*"/>
                    </label>
                </div>
                
                <input type = "submit" name = "registerButton" value = "Register">
                <p class="text-center" id="text-bottom"> <b> Already have an account? </b> <a id="login-text" href="<?= BASEURL ?>/Login"> <b> Login here </b></a> </p>
            </div>
        </form>
    </div>
</body>

</html>