<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?= BASEURL ?>/public/css/buyticket.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Buy Ticket</title>
</head>
<body>
    <div class="container">
        <div class="top-content">
            <a href="<?= BASEURL?>/Details/movieDetails?view-movie=<?=$data['movie'][0]['IDFilm']?>">
                <i class="fa fa-angle-left fa-3x"></i>
            </a>
            <div class="top-content-wrapper">
                <div id="IDAkun"><?= $data['IDAkun'] ?></div>
                <div id="shows-film"><?= $data['movie'][0]['IDShows'] ?></div>
                <div id="judul-film"><?= $data['movie'][0]['Judul'] ?></div>
                <div id="jadwal-film"><?= $data['movie'][0]['nice_date'] ?> - <?= $data['movie'][0]['nice_time'] ?></div>
            </div>
        </div>
        <div class="top-border"></div>
        <div class="bot-content">
            <div class="bot-content-left">
                <div class="select-seats">
                    <form id="form-seats" action="<?= BASEURL?>/Transaction" method="POST">
                        <div class="first-row-seats">
                            <?php for($i=0 ; $i<=9; $i++) { 
                                if($data['seat'][$i]['Status']=='0') {?>
                                <input type="radio" name="seats" id="seats-<?=$i+1?>" value="<?=$i+1?>" onclick="changeSeat()"/>
                            <?php }} ?>

                            <?php for($i=0 ; $i<=9; $i++) {
                                if($data['seat'][$i]['Status']=='0') { ?>
                                <label for="seats-<?=$i+1?>"><?=$i+1?></label>
                            <?php } else { ?>
                                <label for="seats-<?=$i+1?>" id="checked"><?=$i+1?></label>
                            <?php }} ?>
                        </div>

                        <div class="second-row-seats">
                            <?php for($i=10 ; $i<=19; $i++) { 
                                if($data['seat'][$i]['Status']=='0') {?>
                                <input type="radio" name="seats" id="seats-<?=$i+1?>" value="<?=$i+1?>" onclick="changeSeat()"/>
                            <?php }} ?>

                            <?php for($i=10 ; $i<=19; $i++) {
                                if($data['seat'][$i]['Status']=='0') { ?>
                                <label for="seats-<?=$i+1?>"><?=$i+1?></label>
                            <?php } else { ?>
                                <label for="seats-<?=$i+1?>" id="checked"><?=$i+1?></label>
                            <?php }} ?>
                        </div>

                        <div class="third-row-seats">
                            <?php for($i=20 ; $i<=29; $i++) { 
                                if($data['seat'][$i]['Status']=='0') {?>
                                <input type="radio" name="seats" id="seats-<?=$i+1?>" value="<?=$i+1?>" onclick="changeSeat()"/>
                            <?php }} ?>
                            
                            <?php for($i=20 ; $i<=29; $i++) {
                                if($data['seat'][$i]['Status']=='0') { ?>
                                <label for="seats-<?=$i+1?>"><?=$i+1?></label>
                            <?php } else { ?>
                                <label for="seats-<?=$i+1?>" id="checked"><?=$i+1?></label>
                            <?php }} ?>
                        </div>
                    </form>
                    <div id="screen-bar">
                        <div id="screen-text">Screen</div>
                    </div>
                </div>
            </div>
            <div class="mid-border"></div>
            <div class="bot-content-right">
                <div id="booking-summary-title">Booking Summary</div>
                <div id="booking-before-choose">You haven't selected any seat yet. Please click on one of the seat provided</div>
                <div id="booking-judul-film"></div>
                <div id="booking-jadwal-film"></div>
                <div class="booking-seat-harga">
                    <div id="booking-seat"></div>
                    <div id="booking-seat-nomor"></div>
                    <div id="booking-harga"></div>
                </div>
                <button class="trigger" type="button" id="booking-button" onclick="updateSeat()">Buy Ticket</button>
            </div>
            <div class="modal">
                <div class="modal-content">
                    <h1>Payment success!</h1>
                    <div>Thank you for purchasing. You can view your purchase now.</div>
                    <a href="<?= BASEURL ?>/Transaction"><button class="close-button">Go to transaction history</button></a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?= BASEURL ?>/public/js/BuyTicket.js"></script>
    <script>
        modal();
    </script>
</body>
</html>