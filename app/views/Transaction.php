<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href="<?= BASEURL ?>/public/css/transaction.css" rel="stylesheet" type="text/css">

    <title>Transaction</title>
</head>

<body>
    <div class='board'>
        <h1>Transaction History</h1>
        <form class='type3' action='<?= BASEURL ?>/Transaction/goReview'>
            <?php foreach($data['query'] as $v) {
                echo("<div class='row'>");
                echo("<div class='column1'>");
                echo("<img src=" . $v['Film_Foto'] . ">");
                echo("</div>");
                echo("<div class='column2'>");
                echo("<a href=''>" . $v['Judul'] . "</a>");
                echo("<br>");
                echo("<span>Schedule: " . date_format(date_create($v['Date']), 'F, j Y') . " - " . date_format(date_create($v['Time']), 'h.i A') . "</span>");
                echo("<br>");
                echo("</div>");
                if( time() - strtotime($v['Date']) > 0 ) { // sudah lewat
                    echo("<button class='submit'> Add Review </button>");
                }
                else if ( time() - strtotime($v['Date']) == 0) {
                    if( time() - strtotime($v['Time']) > 0 ) { // sudah lewat
                        echo("<button type='submit' class='add'> Add Review </button>");
                    }
                    else {
                        echo("<button type='submit' class='edit'> Edit Review </button>");
                        echo("<button type='button' class='delete'> Delete Review </button>");
                        echo("<span class='submitted'>Your review has been submitted.</span>");
                    }
                }
                else {
                    echo("<button type='submit' class='edit'> Edit Review </button>");
                    echo("<button type='button' class='delete'> Delete Review </button>");
                    echo("<span class='submitted'>Your review has been submitted.</span>");
                }
                echo("</div>");
                echo("<hr>");
            } ?>
        </form>
    </div>
</body>
</html>
