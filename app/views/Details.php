<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?= BASEURL ?>/public/css/details.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <title>Details</title>
</head>
<body>
    <div class="container">
        <div class="top-content">
            <div class="top-left-content">
                <img id="film-image" src="<?= $data['content'][0]['Film_Foto'] ?>">
            </div>
            <div class="top-right-content">
                <div id="judul-content"><?= $data['content'][0]['Judul'] ?></div>
                <div id="genre-minute-content">
                    <div id="genre-content"><?= $data['content'][0]['Genre'] ?></div>
                    |
                    <div id="minute-content"><?= $data['content'][0]['Duration'] ?></div>
                </div>
                <div id="date-content">Released date: <?= $data['content'][0]['Released_Date'] ?></div>
                <div id="rating-content">
                    <i class="fa fa-star"></i>
                    <div id="rating-number"><?= $data['content'][0]['Rating'] ?></div>
                    <div id="rating-max"> /10</div>
                </div>
                <div id="sinopsis-content"><?= $data['content'][0]['Sinopsis'] ?></div>
            </div>
        </div>

        <div class="bot-content">
            <div class="bot-left-content">
                <div id="schedules">
                    Schedules
                </div>
                <table id="schedule-table">
                    <tr id="table-header">
                        <th>Date</th>
                        <th>Time</th>
                        <th>Available Seats</th>
                        <th></th>
                    </tr>
                    <?php foreach($data['schedules'] as $sch) : ?>
                    <tr>
                        <td><?= $sch['nice_date']?></td>
                        <td><?= $sch['nice_time']?></td>
                        <td id="available-seats"><?= $sch['AvailableSeats']?> seats</td>
                        <?php if ($sch['AvailableSeats'] <= 0 || strtotime($sch['nice_date']) < strtotime($data['date'])) { ?>
                            <td id="status-seats-not-available">Not Available <i class="fa fa-times-circle"></i></td>
                        <?php 
                        } else { ?>
                            <form action="<?= BASEURL ?>/BuyTicket/book" method="GET">
                                <td id="status-seats-available"> 
                                    <button type="submit" name="movie" class="button-book" value="<?= $sch['IDShows'] ?>">Book Now</button>
                                    <i class="fa fa-chevron-circle-right"> 
                                </td>
                            </form>
                        <?php } ?>
                    </tr>
                    <?php endforeach;?>
                </table>
            </div>
            
            <div class="bot-right-content">
                <div id="reviews">
                    Reviews
                </div>
                <ul id="reviews-all">
                    <?php foreach($data['reviews'] as $rvw) : ?>
                    <li id="reviews-container">
                        <div id="reviews-list">
                            <img id="image-review" src="<?= $rvw['Akun_Foto']?>">
                            <div id="content-review">
                                <div id="username-review"><?= $rvw['Nama'] ?></div>
                                <div class="rating-review">
                                    <i class="fa fa-star"></i>
                                    <div id="rating-number"><?= $rvw['Rating'] ?></div>
                                    <div id="rating-max">/10</div>
                                </div>
                                <div id="isi-review"><?= $rvw['Review'] ?></div>
                            </div>
                        </div>
                    <div id="border-bot"></div>
                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>