<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Review</title>
    <link href="<?= BASEURL ?>/public/css/review.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="background">
        <div class="outer">
            <p class="title"><img class="button" src="<?= BASEURL ?>/public/img/chevron-left-solid.svg" alt="back-button">
                <?= $data['query'][0]['Judul'] ?>
            </p>
            <div class="inner">
                <form action="<?= BASEURL ?>/UserReview/reviewMovie" method="post">
                    <table>
                        <tr>
                            <th scope="col">Add Rating</th>
                            <td>
                                <div class="rate">
                                    <input type="radio" id="star10" name="rate" value="10"> <label for="star10" title="text">10 stars</label>
                                    <input type="radio" id="star9" name="rate" value="9"> <label for="star9" title="text">9 stars</label>
                                    <input type="radio" id="star8" name="rate" value="8"> <label for="star8" title="text">8 stars</label>
                                    <input type="radio" id="star7" name="rate" value="7"> <label for="star7" title="text">7 stars</label>
                                    <input type="radio" id="star6" name="rate" value="6"> <label for="star6" title="text">6 stars</label>
                                    <input type="radio" id="star5" name="rate" value="5"> <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="rate" value="4"> <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="rate" value="3"> <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="rate" value="2"> <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="rate" value="1"> <label for="star1" title="text">1 star</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="col">Add Review</th>
                            <td>
                                <textarea rows="4" cols="50" name="review" required maxlength='256' minlength='1'></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class='button'>
                                <button class='submit'>Submit</button>
                                <a href="<?= BASEURL ?>/Homepage">
                                    <button class='cancel' type='button'>Cancel</button>
                                </a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</body>

</html>