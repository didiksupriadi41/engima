<?php 

class Database {

    /* Variabel */
    private $db_host = DB_HOST;
    private $db_user = DB_USER;
    private $db_pass = DB_PASSWORD;
    private $db_name = DB_NAME;

    private $db;
    private $stmt;

    /* Konstruktor */
    public function __construct()
    {
        $dbq = 'mysql:host=' . $this->db_host . ';dbname=' . $this->db_name;
        $option = [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        try {
            $this->db = new PDO($dbq, $this->db_user, $this->db_pass);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /* Query setter */
    public function query($query)
    {
        $this->stmt = $this->db->prepare($query);
    }

    /* Executing query */
    public function execute()
    {
        $this->stmt->execute();
    }

    /* Fetch all result */
    public function resultSet()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /* Fetch single result */
    public function resultSingle()
    {
        $this->execute();
        $this->stmt->fetch(PDO::FETCH_ASSOC);
    }
}

?>