<?php
    class Cookie{
        private $key;
        private $value;
        private $expired;
        private $path;

        public function __construct($key = '', $value = '', $expired = 0, $path = '/'){
            $this->key = $key;
            $this->value = $value;
            $this->expired = $expired;
            $this->path = $path;  
        }

        public function setCookie($key, $value, $expired){
            setcookie($key, $value, $expired, "/");
        }

        public function deleteCookie($key){
            $msg = $_COOKIE[$key];
            unset($_COOKIE[$key]);
            setCookie($key,$msg, time()-100000000, "/");
        }

        public function getCookieFromBrowser(){
            if (isset($_COOKIE)){
                foreach($_COOKIE as $key=>$val){
                    $cek = $key.strpos($key, "idUser");
                    if (!($cek)){
                        //do nothing
                    }else if ($cek >= 0) {
                        return $key;
                    }
                }
                return "";
            }
            return "";
        }
    }
?>