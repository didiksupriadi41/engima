<?php

class Transaction extends Controller
{

    public function index()
    {
        $this->view('Template/Navbar');
        $id = $this->model('LoginModel')->findWhoIsLoginById();
        $data['query'] = $this->model('TransactionModel')->transactionList($id);
        $this->view('Transaction',$data);
    }

    public function goReview()
    {
        $id = $this->model('LoginModel')->findWhoIsLoginById();
        $data['query'] = $this->model('TransactionModel')->transactionList($id);
        $this->view('Template/Navbar');
        $this->view('UserReview', $data);
    }
}
