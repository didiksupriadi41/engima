<?php 

class Login extends Controller {

    public function index()
    {
        $this->view('Auth/Login');
    }

    public function checkAccount(){
        if ($this->model('LoginModel')->isAccountExist()){
            $this->model('LoginModel')->addUserLogin();
            header("location: " .BASEURL. "/Homepage");
        }else {
            $data['failed'] = true;
            $data['email'] = $_POST['email'];
            $data['psw'] = $_POST['psw'];
            $this->view('Auth/Login', $data);
        }
    }
}

?>