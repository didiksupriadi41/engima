<?php 

class Logout extends Controller {

    public function index()
    {
        $this->deleteCookie();
        header("location: " .BASEURL. "/Homepage");
    }
    
    public function deleteCookie(){
       $id = $this->model('LoginModel')->findWhoIsLoginById();
       $this->model('LogoutModel')->deleteCookieInBrowser($id);
       $this->model('LogoutModel')->deleteCookieInDatabase($id);
    }
}

?>