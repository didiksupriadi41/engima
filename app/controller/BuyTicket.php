<?php 

class BuyTicket extends Controller {

    public function book()
    {
        $this->view('Template/Navbar');
        $data['movie'] = $this->model('BuyTicketModel')->searchMovie();
        $data['seat'] = $this->model('BuyTicketModel')->findSeat();
        $data['IDAkun'] = $this->model('LoginModel')->findWhoIsLoginById();
        $this->view('BuyTicket',$data);
    }

    public function update()
    {
        $res = $this->model('BuyTicketModel')->updateSeat();
        $ins = $this->model('BuyTicketModel')->updateTable();
    }
}

?>