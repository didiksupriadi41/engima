<?php 

class Homepage extends Controller {

    public function index()
    {
        if ($this->model('LoginModel')->CheckLogged()){
            $data['query'] = $this->findAllMovieToDisplay();
            $data['count'] = count($data['query']);
            $this->view('Homepage', $data);
            $this->view('Template/Navbar');
        }else {
            header("location: " .BASEURL. "/Login");
        }
    }

    public function findAllMovieToDisplay(){
        $res = $this->model('HomepageModel')->findAllMoviePlayingNow();
        return $res;
    }

}

?>