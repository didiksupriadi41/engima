<?php 

class Searchpage extends Controller {

    public function index()
    {
        $this->view('Template/Navbar');
        $this->view('Searchpage');
    }

    public function findMovie()
    {
        $this->view('Template/Navbar');
        $data['query'] = $this->model('SearchModel')->find();  
        $data['countrow'] = $this->model('SearchModel')->countTotalRow();
        $this->view('Searchpage',$data);
    }
}

?>