<?php 

class Details extends Controller {

    public function index()
    {
        $this->view('Template/Navbar');
        $this->view('Details');
    }

    public function movieDetails()
    {
        $data['content'] = $this->model('DetailsModel')->detailContent();
        $data['schedules'] = $this->model('DetailsModel')->detailSchedules();
        $data['reviews'] = $this->model('DetailsModel')->detailReviews();
        $data['date'] = $this->model('DetailsModel')->getTime();
        $this->view('Template/Navbar');
        $this->view('Details',$data);
    }

}

?>