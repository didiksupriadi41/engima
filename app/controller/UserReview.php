<?php

class UserReview extends Controller
{

    public function index()
    {
        $data = $this->model('LoginModel')->findWhoIsLoginById();
        $this->view('Template/Navbar');
        $this->view('UserReview');
    }

    public function reviewMovie()
    {
        $id = $this->model('LoginModel')->findWhoIsLoginById();
        $this->model('UserReviewModel')->createReview();
        $this->view('Template/Navbar');
        $data['query'] = $this->model('TransactionModel')->transactionList($id);
        $this->view('Transaction',$data);
    }
}
