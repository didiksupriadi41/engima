<?php

class LogoutModel{
    private $db;
    private $cookie;

    public function __construct(){
        $this->db = new Database();
        $this->cookie = new Cookie();
    }

    public function deleteCookieInBrowser($id){
        $key = $id."idUser";
        $this->cookie->deleteCookie($key);
    }

    public function deleteCookieInDatabase($id){
        $this->db->query("DELETE FROM Akun_Cookie
                          WHERE Akun_Cookie.IDAkun = '$id'");
        $this->db->resultSingle();   
    }
}

?>