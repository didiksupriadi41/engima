<?php

class LoginModel{
    private $db;
    private $cookie;

    public function __construct(){
        $this->db = new Database;
        $this->cookie = new Cookie;
    }
    
    public function findIdUser(){
        if (isset($_POST['email'])){
            $email = $_POST['email'];
        }
        $this->db->query("SELECT * FROM Akun_Info WHERE Akun_Info.Email = '$email' ");
        $result = $this->db->resultSet();
        return $result;
    }

    public function checkLoggedInDatabase($idUser, $cookie){
        $this->db->query("SELECT * FROM Akun_Cookie
                          WHERE IDAkun = '$idUser' and 
                          Cookie = '$cookie'");
        $result = $this->db->resultSet();
        return (count($result) > 0);
    }
    
    public function checkLogged(){
       $userId = $this->cookie->getCookieFromBrowser();
       if ($userId == '') return false;
       return $this->checkLoggedInDatabase($userId, $_COOKIE[$userId]);
    }

    public function findWhoIsLogin(){
        $idUser = $this->cookie->getCookieFromBrowser();
        $idUser = str_replace('idUser', '', $idUser);
        if ($idUser == '') return "none";
        else {
            $this->db->query("SELECT Username FROM Akun WHERE IDAkun = '$idUser'");
            return $this->db->resultSet()[0]['Username'];
        }    
    }

    public function findWhoIsLoginById(){
        $idUser = $this->cookie->getCookieFromBrowser();
        $idUser = str_replace('idUser', '', $idUser);
        return $idUser;
    }

    public function addUserLoginToDatabase($userId, $cookie){
        $this->db->query("INSERT INTO Akun_Cookie VALUES ('$userId','$cookie')");
        $this->db->resultSingle();
    }

    public function addUserLogin(){
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');
        $userId = ($this->findIdUser())[0]['IDAkun'];
        $userCookie = $userId."idUser";
        $randomString = (string)$date.uniqid();
        $this->cookie->setcookie($userCookie, $randomString, time() + 7200);
        $this->addUserLoginToDatabase($userId, $randomString);
    }

    public function isAccountExist(){
        if (isset($_POST['email'])){
            $email = $_POST['email'];
        }
        if (isset($_POST['psw'])){
            $password = $_POST['psw'];
        }
        $this->db->query("SELECT * FROM Akun JOIN Akun_Info USING(IDAkun)
                          WHERE Akun_Info.Email = '$email' AND Akun.Password = '$password'");
        $result = $this->db->resultSet();
        return count($result) > 0;
    }
}

?>