<?php 

class SearchModel {

    private $db;
    private $limit = 0;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function find()
    {
        $data = $_GET['search-input'];

        if (isset($_POST['page'])) {
            $this->limit = $this->limit + $_POST['page'];
        }

        $next = 0;
        $back = 0;

        if (isset($_POST['back-page'])) {
            $back = $_POST['back-page'];
        }

        if (isset($_POST['next-page'])) {
            $next = $_POST['next-page'];
        }

        $this->limit = $this->limit+($next)+($back);

        $this->db->query('SELECT * FROM Film WHERE Film.judul LIKE \'%' . $data . '%\' LIMIT 5 OFFSET ' . $this->limit);
        $res = $this->db->resultSet();
        return $res;
    }

    public function countTotalRow()
    {
        $data = $_GET['search-input'];
        $this->db->query('SELECT * FROM Film WHERE Film.judul LIKE \'%' . $data . '%\'');
        $res = $this->db->resultSet();
        return count($res);
    }

}

?>