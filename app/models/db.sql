/* Engima Database */
DROP DATABASE IF EXISTS `engima`;
CREATE DATABASE IF NOT EXISTS `engima`;
USE `engima`;

/* Table Akun*/
DROP TABLE IF EXISTS `Akun`;

CREATE TABLE `Akun` (
  `IDAkun` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IDAkun`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Akun` (`IDAkun`, `Username`, `Password`) VALUES (1, 'pandyakaa', '12345');
INSERT INTO `Akun` (`IDAkun`, `Username`, `Password`) VALUES (2, 'irfansofyana', '12345');
INSERT INTO `Akun` (`IDAkun`, `Username`, `Password`) VALUES (3, 'didiksupriadi', '12345');

/* Table Akun_Info */
DROP TABLE IF EXISTS `Akun_Info`;

CREATE TABLE `Akun_Info` (
  `IDAkun` int(11) NOT NULL,
  `Akun_Foto` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone_Number` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDAkun`),
  CONSTRAINT `Akun_Info_ibfk_1` FOREIGN KEY (`IDAkun`) REFERENCES `Akun` (`IDAkun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Akun_Info` (`IDAkun`, `Akun_Foto`, `Phone_Number`, `Email`) VALUES (1, 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', '085707507570', 'pandyakaa@gmail.com');
INSERT INTO `Akun_Info` (`IDAkun`, `Akun_Foto`, `Phone_Number`, `Email`) VALUES (2, 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', '085707507571', 'irfansofyana@gmail.com');
INSERT INTO `Akun_Info` (`IDAkun`, `Akun_Foto`, `Phone_Number`, `Email`) VALUES (3, 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', '085707507572', 'didiksupriadi@gmail.com');

/* Table Akun_Cookie */
DROP TABLE IF EXISTS `Akun_Cookie`;

CREATE TABLE `Akun_Cookie` (
  `IDAkun` int(11) NOT NULL,
  `Cookie` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDAkun`),
  CONSTRAINT `Akun_Cookie_ibfk_1` FOREIGN KEY (`IDAkun`) REFERENCES `Akun` (`IDAkun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/* Table Film */
CREATE TABLE `Film` (
  `IDFilm` int(11) NOT NULL AUTO_INCREMENT,
  `Judul` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Genre` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Duration` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Released_Date` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Film_Foto` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `Sinopsis` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDFilm`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (1, 'Marvel: Avengers', 'Action', '187 mins', '18 April 2018', 'http://localhost/tugas-besar-1-2019/public/img/avengers-endgame.jpeg', 7, 'Vel omnis tenetur omnis maiores.');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (2, 'Spiderman 3', 'Drama', '150 mins', '18 Maret 2018', 'http://localhost/tugas-besar-1-2019/public/img/spiderman.png', 7, 'Eum voluptas non officia illum tenetur quibusdam earum.');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (3, 'Iron Man 3', 'Sci-Fi', '123 mins', '18 Februari 2018', 'http://localhost/tugas-besar-1-2019/public/img/ironman3.png', 9, 'Nulla quibusdam debitis et molestiae atque.');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (4, 'X-Men 1', 'Sci-Fi', '123 mins', '18 Januari 2018', 'http://localhost/tugas-besar-1-2019/public/img/xmen1.png', 9, 'Maecenas erat neque, dignissim euismod ligula sit amet, consequat efficitur sem. Vivamus iaculis, risus vulputate aliquet ornare, lacus sem varius enim, eget scelerisque ante ipsum sit amet nunc. Sed ac euismod ipsum. Integer eget mauris sit amet tellus faucibus congue. Nam scelerisque commodo mollis.');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (5, 'Gundala', 'Horror', '133 mins', '18 Desember 2018', 'http://localhost/tugas-besar-1-2019/public/img/gundala.jpg', 9, 'Nunc laoreet, magna vitae blandit ultrices, lacus justo tempor est, a semper orci sem dignissim nibh. Pellentesque interdum vel neque vitae imperdiet');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (6, 'Harry Potter 1', 'Comedy', '125 mins', '18 Mei 2018', 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', 9, 'Vestibulum rutrum elit nec ornare hendrerit. Morbi semper mattis turpis sed tristique. Nunc accumsan vulputate ultricies. Duis vitae lectus nisl..');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (7, 'Harry Potter 2', 'Biography', '132 mins', '18 April 2018', 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', 9, ' Praesent sodales, mauris id consectetur tempus, est turpis malesuada mauris, quis malesuada tortor felis tempus velit.');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (8, 'Harry Potter 3', 'Action', '135 mins', '18 September 2018', 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', 9, 'Suspendisse vitae mattis eros, ut ornare eros. Aenean sed metus pulvinar leo pretium elementum. Phasellus ultricies ipsum ut vulputate lobortis.');
INSERT INTO `Film` (`IDFilm`, `Judul`, `Genre`, `Duration`, `Released_Date`, `Film_Foto`, `Rating`, `Sinopsis`) VALUES (9, 'Harry Potter 4', 'Sport', '165 mins', '18 Oktober 2018', 'http://localhost/tugas-besar-1-2019/public/img/panda.jpg', 9, 'Donec imperdiet nulla tellus, tempus fringilla enim efficitur eu. Sed quam arcu, rhoncus et commodo ut, hendrerit eu massa.');
/* Table Film_Reviews */
DROP TABLE IF EXISTS `Film_Reviews`;

CREATE TABLE `Film_Reviews` (
  `IDFilm` int(11) DEFAULT NULL,
  `IDAkun` int(11) DEFAULT NULL,
  `Nama` varchar(256) DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `Review` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `IDFilm` (`IDFilm`),
  KEY `IDAkun` (`IDAkun`),
  CONSTRAINT `Film_Reviews_ibfk_1` FOREIGN KEY (`IDFilm`) REFERENCES `Film` (`IDFilm`),
  CONSTRAINT `Film_Reviews_ibfk_2` FOREIGN KEY (`IDAkun`) REFERENCES `Akun` (`IDAkun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (1, 3, 'Didik Supriadi', 7, 'Deserunt dolore inventore non ut.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (2, 3, 'Didik Supriadi', 7, 'Deserunt dolore inventore non ut.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (3, 3, 'Didik Supriadi', 7, 'Deserunt dolore inventore non ut.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (1, 2, 'Irfan Sofyana Putra', 8, 'Maiores consequuntur voluptatem aut sapiente voluptatem iusto minima.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (2, 2, 'Irfan Sofyana Putra', 8, 'Maiores consequuntur voluptatem aut sapiente voluptatem iusto minima.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (3, 2, 'Irfan Sofyana Putra', 8, 'Maiores consequuntur voluptatem aut sapiente voluptatem iusto minima.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (3, 1, 'Pandyaka Aptanagi', 9, 'Officiis ut aut odit mollitia optio.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (2, 1, 'Pandyaka Aptanagi', 9, 'Officiis ut aut odit mollitia optio.');
INSERT INTO `Film_Reviews` (`IDFilm`, `IDAkun`, `Nama`, `Rating`, `Review`) VALUES (1, 1, 'Pandyaka Aptanagi', 9, 'Officiis ut aut odit mollitia optio.');

/* Table Film_Schedule */
DROP TABLE IF EXISTS `Film_Schedule`;

CREATE TABLE Film_Schedule (
    `IDShows` int(11) NOT NULL,
    `IDFilm` int(11) DEFAULT NULL,
    `Date` date COLLATE utf8_unicode_ci DEFAULT NULL,
    `Time` time COLLATE utf8_unicode_ci DEFAULT NULL,
    `AvailableSeats` int(11),
    PRIMARY KEY (`IDShows`) ,
    CONSTRAINT `Film_Schedule_ibfk_1` FOREIGN KEY (`IDFilm`) REFERENCES `Film`(`IDFilm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (1, 1, '2019-09-07', '21:30', 10);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (2, 1, '2019-10-01', '23:30', 14);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (3, 2, '2019-10-01', '16:30', 20);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (4, 3, '2019-10-01', '21:30', 15);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (5, 4, '2019-10-01', '20:30', 15);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (6, 5, '2019-10-01', '14:30', 15);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (7, 3, '2019-10-08', '10:30', 17);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (8, 1, '2019-10-09', '21:30', 0);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (9, 1, '2019-10-30', '05:30', 4);
INSERT INTO `Film_Schedule` (`IDShows`, `IDFilm`, `Date`, `Time`, `AvailableSeats`) VALUES (10, 1, '2019-11-10', '21:30', 5);

/* Table Transaction_History */
DROP TABLE IF EXISTS `Transaction_History`;

CREATE TABLE `Transaction_History` (
  `IDAkun` int(11) DEFAULT NULL,
  `IDShows` int(11) DEFAULT NULL,
  KEY `IDAkun` (`IDAkun`),
  KEY `IDShows` (`IDShows`),
  CONSTRAINT `Transaction_History_ibfk_1` FOREIGN KEY (`IDAkun`) REFERENCES `Akun` (`IDAkun`),
  CONSTRAINT `Transaction_History_ibfk_2` FOREIGN KEY (`IDShows`) REFERENCES `Film_Schedule` (`IDShows`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Transaction_History` (`IDAkun`, `IDShows`) VALUES (1, 3);
INSERT INTO `Transaction_History` (`IDAkun`, `IDShows`) VALUES (2, 2);
INSERT INTO `Transaction_History` (`IDAkun`, `IDShows`) VALUES (3, 1);

/* Table Seats */
DROP TABLE IF EXISTS `Seats`;

CREATE TABLE `Seats` (
  `IDShows` int(11) NOT NULL,
  `Seat_Number` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  KEY `IDShows` (`IDShows`),
  CONSTRAINT `Seats_ibfk_1` FOREIGN KEY (`IDShows`) REFERENCES `Film_Schedule` (`IDShows`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 1, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 2, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 3, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 4, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 5, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 6, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 7, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 8, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 9, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 10, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 11, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 12, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 13, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 14, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 15, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 16, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 17, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 18, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 19, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 20, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 21, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 22, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 23, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 24, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 25, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 26, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 27, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 28, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 29, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (1, 30, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 1, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 2, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 3, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 4, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 5, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 6, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 7, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 8, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 9, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 10, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 11, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 12, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 13, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 14, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 15, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 16, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 17, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 18, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 19, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 20, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 21, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 22, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 23, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 24, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 25, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 26, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 27, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 28, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 29, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (2, 30, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 1, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 2, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 3, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 4, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 5, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 6, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 7, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 8, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 9, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 10, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 11, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 12, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 13, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 14, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 15, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 16, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 17, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 18, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 19, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 20, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 21, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 22, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 23, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 24, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 25, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 26, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 27, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 28, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 29, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (3, 30, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 1, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 2, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 3, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 4, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 5, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 6, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 7, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 8, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 9, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 10, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 11, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 12, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 13, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 14, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 15, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 16, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 17, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 18, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 19, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 20, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 21, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 22, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 23, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 24, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 25, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 26, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 27, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 28, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 29, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (4, 30, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 1, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 2, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 3, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 4, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 5, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 6, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 7, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 8, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 9, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 10, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 11, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 12, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 13, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 14, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 15, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 16, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 17, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 18, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 19, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 20, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 21, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 22, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 23, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 24, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 25, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 26, 0);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 27, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 28, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 29, 1);
INSERT INTO `Seats` (`IDShows`, `Seat_Number`, `Status`) VALUES (9, 30, 0);