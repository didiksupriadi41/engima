<?php
    class HomepageModel{
        private $db;

        public function __construct(){
            $this->db = new Database();
        }

        public function findAllMoviePlayingNow(){
            date_default_timezone_set('Asia/Jakarta');
            $dateNow = date('Y-m-d');
            $timeNow = date('H:i');
            $this->db->query("SELECT IDFilm, Judul, Film_Foto, Rating
                              FROM Film_Schedule JOIN Film USING (IDFilm)
                              WHERE Film_Schedule.Date = '$dateNow'
                              AND TIME_TO_SEC(Film_Schedule.Time) >= TIME_TO_SEC('$timeNow')");
            $res = $this->db->resultSet();
            return $res;
        }
    }
?>