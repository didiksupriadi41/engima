<?php

class UserReviewModel
{
    private $db;
    private $data;

    public function __construct()
    {
        $this->db = new Database;
    }
    public function getReviewIDFilm()
    {
        $query = 'SELECT IDFilm FROM Transaction_History NATURAL JOIN Film_Schedule WHERE IDAkun=' . $this->data;
        $this->db->query($query);
        $result = $this->db->resultSet();
        return $result;
    }
    public function getReviewName()
    {
        $query = 'SELECT Username FROM Akun WHERE IDAkun=' . $this->data;
        $this->db->query($query);
        $result = $this->db->resultSet();
        return $result;
    }
    public function createReview()
    {
        $film = $this->getReviewIDFilm()[0]['IDAkun'];
        $name = $this->getReviewName()[0]['Username'];
        $query = 'INSERT INTO Film_Reviews (IDFilm , IDAkun , Nama , Rating , Review ) VALUES (' . $film . ', ' . $this->data . ', \'' . $name . '\', ' . $_POST['rate'] . ', \'' . $_POST['review'] . '\')';
        $this->db->query($query);
        $this->db->resultSingle();
    }
}
