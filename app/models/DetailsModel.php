<?php 

class DetailsModel {

    private $db;
    private $data;

    public function __construct()
    {
        $this->db = new Database;
        $this->data = $_GET['view-movie'];
    }

    public function detailContent()
    {
        $query = 'SELECT * FROM Film WHERE Film.IDFilm=\''. $this->data . '\'';
        $this->db->query($query);
        $result = $this->db->resultSet();
        return $result;
    }

    public function detailSchedules()
    {
        $query = 'SELECT *, DATE_FORMAT(Date,\'%M %d, %Y\') as nice_date, TIME_FORMAT(Time, \'%h %i %p\') as nice_time FROM Film_Schedule WHERE Film_Schedule.IDFilm=\''. $this->data . '\'';
        $this->db->query($query);
        $result = $this->db->resultSet();
        return $result;
    }

    public function detailReviews()
    {
        $query = 'SELECT * FROM Film_Reviews NATURAL JOIN Akun_Info WHERE Film_Reviews.IDFilm LIKE \'%'. $this->data . '%\'';
        $this->db->query($query);
        $result = $this->db->resultSet();
        return $result;
    }

    public function getTime()
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('m/d/Y h:i:s a');
        return $date;
    }

}

?>