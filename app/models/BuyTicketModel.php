<?php 

class BuyTicketModel {

    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function searchMovie()
    {
        $shows = $_GET['movie'];
        $this->db->query('SELECT *, DATE_FORMAT(Date,\'%M %d, %Y\') as nice_date, TIME_FORMAT(Time, \'%h %i %p\') as nice_time FROM film_schedule NATURAL JOIN Film WHERE IDShows = \'' . $shows . '\'' );
        $res = $this->db->resultSet();

        return $res;
    }

    public function findSeat()
    {
        $shows = $_GET['movie'];
        $this->db->query('SELECT * FROM seats WHERE IDShows = \'' . $shows . '\'');
        $res = $this->db->resultSet();

        return $res;
    }

    public function updateSeat()
    {
        $query = file_get_contents('php://input');
        $query = (array) json_decode($query);
        $seat = $query['seat'];
        $shows = $query['shows'];
        $this->db->query('UPDATE seats SET Status=\'1\' WHERE IDShows=\'' . $shows . '\' and Seat_Number=\'' . $seat . '\'');
        $this->db->execute();
        return 'success';
    }

    public function updateTable()
    {
        $query = file_get_contents('php://input');
        $query = (array) json_decode($query);
        $akun = $query['IDAkun'];
        $shows = $query['shows'];
        $this->db->query('INSERT INTO transaction_history (IDAkun, IDShows) VALUES (' . $akun . ',' . $shows . ')');
        $this->db->execute();
        return 'success';
    }

}

?>