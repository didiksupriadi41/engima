<?php

class RegisterModel{
    private $db;

    public function __construct(){
        $this->db = new Database(); 
    }

    public function findTheNumberOfUser(){
        $this->db->query("SELECT * FROM Akun");
        $res = $this->db->resultSet();
        return count($res);
    }

    public function checkUsernameAvailable(){
        $username = $_POST['username'];
        $this->db->query("SELECT * FROM Akun WHERE Akun.Username = '$username'");
        $result = $this->db->resultSet();
        return count($result) > 0;
    }

    public function addUserToAkunDatabase(){
        $idAkun = $this->findTheNumberOfUser() + 1;
        $username = $_POST['username'];
        $password = $_POST['password'];
        $this->db->query("INSERT INTO Akun VALUES('$idAkun', '$username', '$password')");
        $this->db->resultSingle();
    }

    public function addUserToAkunInfoDatabase(){
        $idAkun = $this->findTheNumberOfUser();
        $photo = $_POST['file'];
        $phoneNumber = $_POST['phoneNumber'];
        $email = $_POST['email'];
        $this->db->query("INSERT INTO Akun_Info VALUES('$idAkun', '$photo', '$phoneNumber', '$email')");
        $this->db->resultSingle();
    }

    public function addUserToDatabase(){
        $this->addUserToAkunDatabase();
        $this->addUserToAkunInfoDatabase();
    }


}

?>