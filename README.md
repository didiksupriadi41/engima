# Tugas 1 IF3110 Pengembangan Aplikasi Berbasis Web

## Deskripsi Singkat

Saat ini Engi ingin melakukan ekspansi usaha dengan membangun sebuah bioskop. Setelah berdiri selama 2 bulan, Engi merasa bioskopnya sepi dibanding bioskop kompetitor lainnya. Usut punya usut, ternyata faktor utama penyebab sepinya bioskop Engi adalah tidak adanya kemudahan pemesanan tiket secara daring. Oleh karena itu, Engi meminta Anda untuk membuat aplikasi web pemesanan tiket bioskop daring. Engi menginginkan website miliknya untuk memiliki fitur pembelian tiket, memberi ulasan pada film yang telah ditonton, login dan register, riwayat pembelian tiket.

Engi telah mempekerjakan seorang UI/UX profesional untuk mendesain tampilan *website*-nya. Anda diminta untuk mengimplementasikan tampilan sedemikian mirip dengan tampilan pada contoh. Engi tidak meminta Anda untuk membangun *website* yang *responsive*. Icon dan jenis font tidak harus sama dengan contoh. Tata letak elemen, warna font, garis pemisah, dan perbedaan ukuran font harus terlihat sesuai contoh. Format rating dan waktu harus terlihat sesuai contoh tampilan.

## Daftar Requirement

* Javascript
* HTML
* CSS
* PHP
* Integrated XAMPP atau LAMP

## Cara Instalasi

1. Download XAMPP.
2. Buka Aplikasi XAMPP.
3. Instal dengan pengaturan default.
4. Jalankan XAMPP Control Panel.
5. Jalankan komponen Apache and MySQL.

## Cara Menjalankan Server

1. Masukkan file project ini pada folder xampp/htdocs
2. Jalankan localhost/<nama-folder>

## Screenshot Tampilan Aplikasi

1. Login page

![login](/uploads/b03f93920a07d13c48b7e1684f406deb/login.png)

2. Register Page

![register](/uploads/48836426e240efd6d61df79d74c2820f/register.png)

3. Search Page

![searchpage](/uploads/a28899340307ae70c64743534a75497d/searchpage.png)

4. Detail Page

![detail](/uploads/786ad7628142dba4384f1b8c639a4dd3/detail.png)

5. Booking before

![book-before](/uploads/ecc66009bbd2413c23e3ccf40279078c/book-before.png)

6. Booking after

![book-after](/uploads/1efa8115dd1b903f2ce42087054cd983/book-after.png)

7. Model

![model](/uploads/5760d42d738c64152b2e6605cf020dea/model.png)

## Pembagian Tugas

### Frontend

1. Login: 13517078
2. Register: 13517078
3. Home: 13517078
4. Search Result: 13517003
5. Film Detail: 13517003
6. Buy Ticket: 13517003
7. Transaction History: 13517069
8. User Review: 13517069

### Backend

+ Major

    1. Login: 13517078
    2. Register: 13517078
    3. Home: 13517078
    4. Search Result: 13517003
    5. Film Detail: 13517003
    6. Buy Ticket: 13517003
    7. Transaction History: 13517069
    8. User Review: 13517069

+ Minor (database, code fix, api, etc.)

    - 13517003, 13517078, 13517069


## About

K3 IF3110

Pandyaka Aptanagi | Didik Supriadi | Irfan Sofyana Putra

13517003 | 13517069 | 13517078
