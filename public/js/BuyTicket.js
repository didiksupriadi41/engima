function changeSeat() {

    changeView();
    var radios = document.getElementsByName('seats');
    var seat = 0;
    for (var i=0 ; i < radios.length ; i++) {
        if (radios[i].checked==true) {
            seat = radios[i].value;
        }
    }
    document.getElementById("booking-seat").innerHTML = "Seat #".concat(seat);
    document.getElementById("booking-seat-nomor").innerHTML = seat;
    document.getElementById("booking-harga").innerHTML = "Rp 45.000";
}

function changeView() {
    document.getElementById('booking-judul-film').innerHTML = document.getElementById('judul-film').innerHTML;
    document.getElementById('booking-jadwal-film').innerHTML = document.getElementById('jadwal-film').innerHTML;
    document.getElementById('booking-before-choose').innerHTML = "";
    document.getElementById('booking-button').style.display = "initial";
}

function modal(){
    var modal = document.querySelector(".modal");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
        modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }

    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
}

function updateSeat() {
    var url = 'http://localhost/tugas-besar-1-2019/BuyTicket/update';
    fetch(url,{
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({ 
            seat: document.getElementById('booking-seat-nomor').innerHTML,
            shows: document.getElementById('shows-film').innerHTML,
            IDAkun: document.getElementById('IDAkun').innerHTML
        })
    }).then(response=>console.log(response.body));
}